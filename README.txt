<p><h1>AlFramework Project</h1></p>
	<p>
	This is a project designed to use learn standard framework systems. 
	This is but one stepping stone on the path of becoming a software engineer
	as opposed to a framework consumer. 
	</p>
	<p>
	Alongside the framework project I will begin documenting my progress and learning
	through my youtube channel, which you can follow along with here:
	TODO: Implement the link to my youtube channel.
	</p>
	<p>
	Within this project I have a few key features I want to properly work on,
	this includes: </br>
		<ol>
		<li>
		The ability to utilize pre-built design patterns in existing frameworks
		</li>
		<li>
		A better understanding of different open source softwares including
		open broadcast software and open video editor.
		</li>
		<li>
		An understanding of multithreading and how I can combine it with different
		task to create a proper application.
		</li>
		<li>
		An understanding of open source software and online repositories for
		cross-platform developing and flexibility regarding development
		implementation.
		</li>
		<li>
		A better understanding of both prominent agile software - based methedologies
		</li>
		<li>
		Several streamlined data structures such as a list-array 
		(similar to a vector in STL) and branchable graph system.
		</li>
		<li>
		How to develop tools and plugins for existing closed entry software
		</li>
		<li>
		And many, many other things too!
		</li>
		</ol> 
	</p>
	<p><h2>About Me<h2></p>
	<p>
	Considering this is my first publically maintained code repository, I wanted to 
	write a few things about myself in case you thought I would be a perfect fit as
	a junior software engineer in your company. Yes you, mr / mrs company person, 
	this one is for you!
	</p>
	<p>
	I dont want to bore you with my fascinations dreams and aspirations, so lets cut 
	it into something more digestible. I am:
	<ul>
		<li>
		A univesity student at <a><href = "">The University of Lethbridge</a>.
		Currently in my second year of study.
		</li>
		A profficient user of open source code. I only work with what I can alter, which is why 
		my stack makes heavy use of C++, the best programming language for application and general
		use software development.
		<li>
		A passionate learner. I have so many projects that I juggle, that half of
		my summer was spent learning enough fine art and animation to procedurally
		generate shaders and procedurally animate bones in a character.
		</li>
		<li>
		A mathmatician. Most of my coding projects have been turning math into easily
		use code for future use, part of the reason I am making this current project.
		</li>
	</ul>
	</br>
	I am serious when I say I am crazy about software engineering.
	</p>
	<p><h3>Contact Me:<h3></p>
	<ul>
	<li>
	TODO: put relevant contact information and set up links
	</li>
	<ul>
